==============
GPSDO Exporter
==============

GPSDO Exporter is a Prometheus exporter for Leo Bodnar GPSDOs

--------------
 Requirements
--------------
To install GPSDOExporter's requirements, run the following commands::

    $ sudo apt install python3-pip python3-setuptools
    $ sudo -H pip3 install -r requirements.txt

-------
 Build
-------
To build::

    $ python3 setup.py build

---------
 Install
---------

To install the build::

    $ sudo python3 setup.py install
